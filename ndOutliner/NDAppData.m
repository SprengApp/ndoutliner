//
//  NDAppData.m
//  ndOutliner
//
//  Created by Oliver Sprenger on 01.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NDAppData.h"
#import "NDOutlineElement.h"

@interface NDAppData ()

@end 

@implementation NDAppData

static NDAppData *gInstance = NULL;


#pragma mark Init Functions

+ (NDAppData *)sharedAppData
{ 
    if(gInstance)
        return gInstance;
    
    @synchronized(self)
    {
        if (gInstance == NULL)
            gInstance = [[self alloc] init];
    }
    return gInstance;
}


- (void)initilisation 
{

}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self initilisation];
    }
    return self;
}



@end
