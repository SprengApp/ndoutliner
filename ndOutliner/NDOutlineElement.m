//
//  NDOutlineElement.m
//  ndOutliner
//
//  Created by Oliver Sprenger on 25.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NDOutlineElement.h"

@interface NDOutlineElement ()


@end


@implementation NDOutlineElement

@synthesize text = _text;
@synthesize description = _description;
@synthesize parent = _parent;
@synthesize status = _status;
@synthesize isVisible = _isVisible;

NSMutableArray *_childs;
NDOutlineElement *_parent;


#pragma mark Child Functions

- (NSObject<NDOutlineItem>*)visibleChildAtPosition:(NSInteger)position
{
    NSObject<NDOutlineItem>* foundChild = NULL;
    if (position == 0)
        foundChild = self;
    else {
        for (NSObject<NDOutlineItem>* child in self.childs) {
            if (foundChild == NULL) {
                if (child.countOfVisibleChilds < position) {
                    position = position - child.countOfVisibleChilds;
                }
                else {
                    foundChild = [child visibleChildAtPosition:(position - 1)];
                }
            }
        }
    }
    return foundChild;
}


- (NSInteger)countOfVisibleChilds
{
    NSInteger visibleChildsCount = 0;
    if (self.isVisible) {
        for (NSObject<NDOutlineItem>* child in self.childs) {
            visibleChildsCount = visibleChildsCount + child.countOfVisibleChilds;
        }
    }
    return visibleChildsCount;
}

- (NSObject<NDOutlineItem>*)addChildWithText:(NSString*)text
{
    NDOutlineElement *newChild = [[NDOutlineElement alloc] initWithParent:self];
    newChild.text = text;
    [_childs addObject:newChild];
    return newChild;
}


- (void)removeChild:(NSObject<NDOutlineItem>*)child
{
    [_childs removeObject:child];
    [(NDOutlineElement*)child setParent:NULL];
}


#pragma mark Internale Functions

- (void)setParent:(NSObject<NDOutlineItem>*)outletElement
{
    if (_parent != outletElement) 
    {
        _parent = outletElement;
    }
}



#pragma mark ReadOnly Functions

- (NSArray*)childs
{
    return [_childs copy];
}

-  (NSObject<NDOutlineItem>*)parent
{
    return _parent;
}


#pragma mark Init Functions

- (void)initalisation 
{
    _text = @"";
    _description = @"";
    _parent = NULL;
    _status = NDOutlineItemStatusUnknowed;
    _childs = [[NSMutableArray alloc] init];
}

- (id)initWithParent:(NSObject<NDOutlineItem>*)parent
{
    self = [super init];
    if (self) 
    {
        [self initalisation];
        self.parent = parent;
    }
    return self;
}


@end
