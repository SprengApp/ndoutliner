//
//  NDAppData.h
//  ndOutliner
//
//  Created by Oliver Sprenger on 01.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NDOutlineItem.h"

@interface NDAppData : NSObject


+ (NDAppData *)sharedAppData;

@end
