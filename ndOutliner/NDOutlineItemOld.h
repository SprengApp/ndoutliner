//
//  NDOutlineItem.h
//  ndOutliner
//
//  Created by Oliver Sprenger on 07.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    NDOutlineItemStatusNotSelected = 0,
    NDOutlineItemStatusSelected,
    NDOutlineItemStatusUnknowed
} NDOutlineItemStatus;


@protocol NDOutlineItem <NSObject>

@property (nonatomic) NSString *text;
@property (nonatomic) NSString *description;
@property (nonatomic, readonly) NSObject<NDOutlineItem> *parent;
@property (nonatomic, readonly) NSArray *childs;
@property (nonatomic) NDOutlineItemStatus status;
@property (nonatomic) BOOL isVisible;
@property (nonatomic, readonly) NSInteger countOfVisibleChilds;

- (NSObject<NDOutlineItem>*)addChildWithText:(NSString*)text;
- (void)removeChild:(NSObject<NDOutlineItem>*)child;

- (NSObject<NDOutlineItem>*)visibleChildAtPosition:(NSInteger)position;


@end
