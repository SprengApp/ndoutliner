//
//  NDOutlineFile.m
//  ndOutliner
//
//  Created by Oliver Sprenger on 03.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NDOutlineFile.h"
#import "NDOutlineElement.h"



@implementation NDOutlineFile

@synthesize text = _text;
@synthesize description = _description;
@synthesize parent = _parent;
@synthesize status = _status;
@synthesize isVisible = _isVisible;

NSMutableArray *_childs;


#pragma mark Child Functions

- (NSObject<NDOutlineItem>*)visibleChildAtPosition:(NSInteger)position
{
    NSObject<NDOutlineItem>* foundChild = NULL;
    if (position == 0)
        foundChild = self;
    else {
        for (NSObject<NDOutlineItem>* child in self.childs) {
            if (foundChild == NULL) {
                if (child.countOfVisibleChilds < position) {
                    position = position - child.countOfVisibleChilds;
                }
                else {
                    foundChild = [child visibleChildAtPosition:(position - 1)];
                }
            }
        }
    }
    return foundChild;
}

- (NSInteger)countOfVisibleChilds
{
    NSInteger visibleChildsCount = 0;
    if (self.isVisible) {
        for (NSObject<NDOutlineItem>* child in self.childs) {
            visibleChildsCount = visibleChildsCount + child.countOfVisibleChilds;
        }
    }
    return visibleChildsCount;
}

- (NSObject<NDOutlineItem>*)addChildWithText:(NSString*)text
{
    NDOutlineElement *newChild = [[NDOutlineElement alloc] initWithParent:self];
    newChild.text = text;
    [_childs addObject:newChild];
    return newChild;
}


- (void)removeChild:(NSObject<NDOutlineItem>*)child
{
    [_childs removeObject:child];
    [(NDOutlineElement*)child setParent:NULL];
}


#pragma mark ReadOnly Functions

- (NSArray*)childs
{
    return [_childs copy];
}

-  (NSObject<NDOutlineItem>*)parent
{
    return NULL;
}


#pragma mark Init Functions

- (void)initalisation 
{
    _text = @"";
    _description = @"";
    _status = NDOutlineItemStatusUnknowed;
    _parent = NULL;
    _childs = [[NSMutableArray alloc] init];
}

- (id)init
{
    self = [super init];
    if (self) 
    {
        [self initalisation];
    }
    return self;
}



@end
