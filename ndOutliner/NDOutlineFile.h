//
//  NDOutlineFile.h
//  ndOutliner
//
//  Created by Oliver Sprenger on 03.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NDOutlineItem.h"

@interface NDOutlineFile : NSObject <NDOutlineItem>

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSObject<NDOutlineItem> *parent;
@property (nonatomic, readonly) NSArray *childs;
@property (nonatomic, assign) NDOutlineItemStatus status;
@property (nonatomic, assign) BOOL isVisible;
@property (nonatomic, readonly) NSInteger countOfVisibleChilds;

- (NSObject<NDOutlineItem>*)addChildWithText:(NSString*)text;
- (void)removeChild:(NSObject<NDOutlineItem>*)child;
- (NSObject<NDOutlineItem>*)visibleChildAtPosition:(NSInteger)position;

@end
