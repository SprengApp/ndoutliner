//
//  NDAppDelegate.h
//  ndOutliner
//
//  Created by Oliver Sprenger on 25.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NDMainViewController;

@interface NDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NDMainViewController *viewController;

@end
