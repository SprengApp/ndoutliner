//
//  NDViewController.m
//  ndOutliner
//
//  Created by Oliver Sprenger on 25.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NDMainViewController.h"
#import "NDCommonDefaults.h"
#import "NDAppData.h"
#import "NDOutlineElement.h"
#import "NDOutlineItem.h"
#import "NDOutlineFile.h"

@interface NDMainViewController ()

@property (nonatomic, strong) NSObject<NDOutlineItem> *rootOutlineItem;

@end

@implementation NDMainViewController

@synthesize mainNavController = _mainNavController;
@synthesize mainTableView = _mainTableView;
@synthesize mainNavBar = _mainNavBar;
@synthesize rootOutlineItem = _rootOutlineItem;


#define NDPlusCellIdentifier @"NDPlusCell"
#define NDOutlineItemCellIdentifier @"NDOutlineItemCellIdentifier"

#pragma mark Erstellung von UITableCell 

- (UITableViewCell*)plusCellforTableView:(UITableView*)tableView 
{
    UITableViewCell *plusCell;
    plusCell = [tableView dequeueReusableCellWithIdentifier:NDPlusCellIdentifier];
    if (plusCell == NULL) {
        plusCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NDPlusCellIdentifier];
    }
    plusCell.textLabel.textColor = [UIColor grayColor];
    plusCell.textLabel.text = NSLocalizedString(@"AddTableText", @"Add Text");
    return plusCell;
}


- (UITableViewCell*)outlineItemCellforTableView:(UITableView*)tableView atRow:(NSInteger)row
{
    UITableViewCell *itemCell;
    itemCell = [tableView dequeueReusableCellWithIdentifier:NDOutlineItemCellIdentifier];
    if (itemCell == NULL) {
        itemCell =  [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NDPlusCellIdentifier];
    }
    NSObject<NDOutlineItem>* item = [self.rootOutlineItem.childs objectAtIndex:row];
    itemCell.textLabel.text = item.text;
    return itemCell;
}
        

#pragma mark Delegates für UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section > 0) 
    {
         [NSException raise:NSInternalInconsistencyException format:@"tableView cellForRowAtIndexPath Section zu gross:%i", indexPath.section];
    }
    if (indexPath.row > (self.rootOutlineItem.childs.count)) 
    {
        [NSException raise:NSInternalInconsistencyException format:@"tableView cellForRowAtIndexPath Row zu gross:%i", indexPath.row];
    }
    if (indexPath.row == self.rootOutlineItem.childs.count) {
        cell = [self plusCellforTableView:tableView];
    } else {
        cell = [self outlineItemCellforTableView:tableView atRow:indexPath.row];
    }
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.rootOutlineItem.childs.count + 1;
    }
    else {
        [NSException raise:NSInternalInconsistencyException format:@"tableView numberOfRowsInSection:%i",section];
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


#pragma mark View Functions

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.rootOutlineItem = [[NDOutlineFile alloc] init];
    self.rootOutlineItem.text = @"RootOutlineFile";
    [self.rootOutlineItem addChildWithText:@"Test Item"];
    // Do any additional setup after loading the view, typically from a nib.
    self.mainNavController = [[UINavigationController alloc] initWithRootViewController:self];

    
    CGRect rect = CGRectMake(0,0, self.view.frame.size.width, kDefaultNavigationBarHeight);
    self.mainNavBar = [[UINavigationBar alloc] initWithFrame:rect];
    self.mainNavBar.barStyle = UIBarStyleBlack;
    self.mainNavBar.translucent = NO;
    [self.view addSubview:self.mainNavBar];
    
    rect = CGRectMake(0, self.mainNavBar.frame.size.height, self.view.frame.size.width, 
                      self.view.frame.size.height - self.mainNavBar.frame.size.height);
    self.mainTableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.view addSubview:self.mainTableView];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [self.mainTableView removeFromSuperview];
    [self.mainNavController removeFromParentViewController];
    self.mainNavController = NULL;
    self.mainTableView = NULL;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}



@end
