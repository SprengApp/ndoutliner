//
//  NDViewController.h
//  ndOutliner
//
//  Created by Oliver Sprenger on 25.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NDMainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UINavigationController *mainNavController;
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) UINavigationBar *mainNavBar;


@end
